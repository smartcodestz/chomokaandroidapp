package tz.co.smartcodes.bongoflix.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.interfaces.MovieItemClickListener;
import tz.co.smartcodes.bongoflix.models.Movie;


public class PopularMoviesAdapter extends RecyclerView.Adapter<PopularMoviesAdapter.ViewHolder> {

    private List<Movie> movies;
    private Context context;
    private MovieItemClickListener itemClickListener;

    private static final String TMDB_POSTER_URL_SIZE_185 = "http://image.tmdb.org/t/p/w185/";

    public PopularMoviesAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_item_grid, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.originalTitle.setText(movies.get(position).getOriginalTitle());

        viewHolder.releaseDate.setText(
                parseDate(movies.get(position).getReleaseDate())
        );

        viewHolder.voteAverage.setText(
                String.valueOf(movies.get(position).getVoteAverage())
        );

        Glide.with(context)
                .load(TMDB_POSTER_URL_SIZE_185 + movies.get(position).getPosterPath())
                .into(viewHolder.posterImage);
    }

    @Override
    public int getItemCount() {
        return movies != null ? movies.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RoundedImageView posterImage;
        TextView originalTitle, releaseDate, voteAverage;

        ViewHolder(@NonNull View view) {
            super(view);
            posterImage = view.findViewById(R.id.poster);
            originalTitle = view.findViewById(R.id.original_title);
            releaseDate = view.findViewById(R.id.release_date);
            voteAverage = view.findViewById(R.id.vote_average);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void setItemClickListener(MovieItemClickListener clickListener) {
        itemClickListener = clickListener;
    }

    public void addMovies(List<Movie> movieList) {
        movies.addAll(movieList);
    }

    private String parseDate(String releaseDate) {
        try{
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(releaseDate);
            return new SimpleDateFormat("MMM dd, yyyy").format(date);
        }
        catch(Exception e){
            Log.e("TAG", e.getMessage());
        }
        return "";
    }
}
