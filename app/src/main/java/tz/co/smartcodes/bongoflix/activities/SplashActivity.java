package tz.co.smartcodes.bongoflix.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        // startActivity(new Intent(this, MainActivity.class));
        // finish();
        init();
    }

    private void init() {
        SharedPreferences sharedPreferences = BongoFlixUtils.getSharedPreferences(this);
        Intent intent;
        if (sharedPreferences.getBoolean("not_first_run", false))
            intent = new Intent(this, MainActivity.class);
        else
            intent = new Intent(this, TourActivity.class);

        startActivity(intent);
        finish();
    }
}
