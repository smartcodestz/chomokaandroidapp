package tz.co.smartcodes.bongoflix.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BongoFlixNetworkInterface {

    @FormUrlEncoded
    @POST("api/receive-request")
    Call<String> postFavouriteMovieToApi(
            @Header("Authorization") String authKey,
            @Field("data") String data
    );
}
