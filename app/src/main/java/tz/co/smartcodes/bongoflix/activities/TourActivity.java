package tz.co.smartcodes.bongoflix.activities;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.List;

import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.adapters.TourScreenAdapter;
import tz.co.smartcodes.bongoflix.fragments.TourScreenOne;
import tz.co.smartcodes.bongoflix.fragments.TourScreenTwo;

public class TourActivity extends AppCompatActivity {

    ViewPager pager;
    SpringDotsIndicator dotsIndicator;
    TourScreenAdapter adapter;
    List<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);

        pager = findViewById(R.id.viewpager);

        dotsIndicator = findViewById(R.id.dots_indicator);

        fragments = new ArrayList<>();
        fragments.add(new TourScreenOne());
        fragments.add(new TourScreenTwo());
        // fragments.add(new TourScreenThree());

        adapter = new TourScreenAdapter(getSupportFragmentManager(), fragments);

        pager.setAdapter(adapter);

        dotsIndicator.setViewPager(pager);

    }
}
