package tz.co.smartcodes.bongoflix.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.models.Movie;

public class SimilarMoviesAdapter extends PagerAdapter {

    private List<Movie> movies;
    private Context context;

    public SimilarMoviesAdapter(List<Movie> movieList, Context ctx) {
        movies = movieList;
        context = ctx;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, Object object) {
        return (view == object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.movie_item_similar, container,false);

        RoundedImageView similarPosterImage = view.findViewById(R.id.similar_movie_poster);
        // TextView similarTitle = view.findViewById(R.id.similar_movie_title);

        Glide.with(context).load("http://image.tmdb.org/t/p/w185/" + movies.get(position).getPosterPath()).into(similarPosterImage);
        // similarTitle.setText(movies.get(position).getOriginalTitle());

        container.addView(view);
        return view;
    }

    @Override
    public float getPageWidth(int position) {
        return 0.3f;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout)object);
    }
}
