package tz.co.smartcodes.bongoflix.activities;

import android.arch.persistence.room.Room;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.parceler.Parcels;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.adapters.SimilarMoviesAdapter;
import tz.co.smartcodes.bongoflix.database.BongoFlixDatabase;
import tz.co.smartcodes.bongoflix.interfaces.OnGetMoviesCallback;
import tz.co.smartcodes.bongoflix.models.Movie;
import tz.co.smartcodes.bongoflix.models.MoviesResponse;
import tz.co.smartcodes.bongoflix.api.TmdbNetworkClient;
import tz.co.smartcodes.bongoflix.api.TmdbNetworkInterface;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;

public class MovieDetailsActivity extends AppCompatActivity
        implements View.OnClickListener {

    private static final String TMDB_POSTER_URL_SIZE_500 = "http://image.tmdb.org/t/p/w500/";

    TextView detailOriginalTitle, detailOverview;

    ImageView detailPoster, saveAsFavourite;

    ViewPager similarMoviesList;

    TmdbNetworkInterface api;

    private SimilarMoviesAdapter adapter;

    ProgressBar progressBar;

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        movie = Parcels.unwrap(getIntent().getParcelableExtra("movie"));

        detailPoster = findViewById(R.id.movie_detail_poster);

        saveAsFavourite = findViewById(R.id.save_as_favourite);

        detailOriginalTitle = findViewById(R.id.detail_original_title);
        detailOverview = findViewById(R.id.detail_overview);
        similarMoviesList = findViewById(R.id.similar_movies);

        progressBar = findViewById(R.id.progress_similar);

        loadBiggerPoster(); // Loading bigger movie poster from TMBD

        detailOriginalTitle.setText(movie.getOriginalTitle());
        detailOverview.setText(movie.getOverview());

        Retrofit retrofit = TmdbNetworkClient.getRetrofit();
        api = retrofit.create(TmdbNetworkInterface.class);

        // Fetching similar movies from the API
        getSimilarMovies(new OnGetMoviesCallback() {
            @Override
            public void onSuccess(final List<Movie> movies) {
                adapter = new SimilarMoviesAdapter(movies, getApplicationContext());
                similarMoviesList.setAdapter(adapter);
                dismissProgressBar();
            }

            @Override
            public void onError() {
                Toast.makeText(MovieDetailsActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        });

        saveAsFavourite.setOnClickListener(this);

    }

    private void loadBiggerPoster() {
        Glide.with(this).load(TMDB_POSTER_URL_SIZE_500 + movie.getPosterPath()).into(detailPoster);
    }

    private void getSimilarMovies(final OnGetMoviesCallback callback) {
        progressBar.setVisibility(View.VISIBLE);
        api.getSimilarMovies(movie.getId(), BongoFlixUtils.TMDB_API_KEY, "en-US")
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MoviesResponse> call, @NonNull Response<MoviesResponse> response) {
                        if (response.isSuccessful()) {
                            MoviesResponse moviesResponse = response.body();
                            if (moviesResponse != null && moviesResponse.getMovies() != null) {
                                callback.onSuccess(moviesResponse.getMovies());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
                        callback.onError();
                    }
                });
    }

    private void storeIntoDatabaseAsFavourite(final Movie movie) {

        final String DATABASE_NAME = "bongoflix_db";

        final BongoFlixDatabase database = Room.databaseBuilder(getApplicationContext(), BongoFlixDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();

        new Thread(new Runnable() {
            tz.co.smartcodes.bongoflix.database.models.Movie movieModel;
            @Override
            public void run() {
                movieModel = database.daoAccess().fetchOneMoviesbyMovieId(movie.getId());

                if (movieModel != null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Already Your Favourite", Toast.LENGTH_LONG).show();
                        }
                    });

                } else {
                    movieModel = new tz.co.smartcodes.bongoflix.database.models.Movie();
                    movieModel.setId(movie.getId());
                    movieModel.setTitle(movie.getTitle());
                    movieModel.setOverview(movie.getOverview());
                    movieModel.setPosterPath(movie.getPosterPath());

                    database.daoAccess().insertOnlySingleMovie(movieModel);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            saveAsFavourite.setImageResource(R.drawable.ic_saved);
                        }
                    });

                    String title = "", overview = "", posterPath = "";
                    // URL Encoding movie details before posting to an API
                    try {
                        title = URLEncoder.encode(movie.getTitle(), "UTF-8");
                        overview = URLEncoder.encode(movie.getOverview(), "UTF-8");
                        posterPath = URLEncoder.encode(movie.getPosterPath(), "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    String data = title + "," + overview + "," + posterPath;

                    // Hardcoded API Auth key
                    api.postFavouriteMovieToApi("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNkZjAwODI4YzM0NDI5ZjU0NGZjYThmYTFiM2E2MjYyYTFkNzNhM2UyMWY2YmQyNWZiZDBhYTU4NTAxNmYxN2Q4NGMxY2Q0MWVhZjliZmI5In0.eyJhdWQiOiIxIiwianRpIjoiM2RmMDA4MjhjMzQ0MjlmNTQ0ZmNhOGZhMWIzYTYyNjJhMWQ3M2EzZTIxZjZiZDI1ZmJkMGFhNTg1MDE2ZjE3ZDg0YzFjZDQxZWFmOWJmYjkiLCJpYXQiOjE1NDI0NTkzODMsIm5iZiI6MTU0MjQ1OTM4MywiZXhwIjoxNTczOTk1MzgyLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.MRLzJ4x1JFQBkaGuRTQOUC3W5ox9Aq4kMphmbE38Nh_zZL-R0Z_LGxds5MQzz754F309AzoquYTc7osVXVLFoxW9ThlH9seMFBu4qh9W8Hx73x7pEetAKPRqw2Nq8OladDOwYXHhBD3dR4NfNaPe-w_DHcxCqSrkdzv8D1uwfwm_kpnffBcrLTsprSC5um9g6aaBdBvOl4Iy15ABbaHy76iAKVK8SEOg_uwu9wCBjxpCQO6XpW0zVNr96ooU43bczQO2CpV9wDL-AjZu2WZh1eeadzrcxGHhb08WcuTDs7GAP_gEwHnB8Y8ncTu8r8xTVU4aYxQsPmol6jRpQQFxfw98f5Sz-H76Urrz4QZQUZzrBON848_JjT_wufzWNmGqBrUvWdXz8j_Ajt8zZq9IppCjRjwqSZPsheCJgtS48CRj1kpRUu43ci-HTdxi_pQBeB0ywLZ2lkKE-6XDwpFMsJArZjyDzcPfXjTXBbEPXc2L2tMlxjpUhk4aT7dWNF_-NrpLV-r2RmyUw6mUIIi4YX9YY2MM4cigcJmAbOqnC0lWYNgCi4nm7MaNmYTG91vNNBSlTCO6ogEnWFmIOJ3DUhfKXOSn_3I8wfZ6KPyihSswsXCgJkfXcdG6KfefQtIPpMGsqe3kF76vG1z7WnFXZRBNWXKplwy6V7gv_tltnzE", data).enqueue(new Callback<String>() {

                        @Override
                        public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                            if (response.isSuccessful()) {
                                /** final String successResponse = response.body();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // Toast.makeText(getApplicationContext(), successResponse, Toast.LENGTH_LONG).show();
                                    }
                                }); */
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                            t.printStackTrace();
                        }
                    });
                }
            }
        }) .start();
    }

    @Override
    public void onClick(View view) {
        storeIntoDatabaseAsFavourite(movie);
    }

    private void dismissProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

}
