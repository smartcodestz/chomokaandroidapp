package tz.co.smartcodes.bongoflix.api;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import tz.co.smartcodes.bongoflix.models.MoviesResponse;

public interface TmdbNetworkInterface {

    String CHOMOKA_DEMO_BASE_URL = "http://54.245.220.198/chomokademo/";

    @GET("movie/popular")
    Call<MoviesResponse> getPopularMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    @GET("movie/{id}/similar")
    Call<MoviesResponse> getSimilarMovies(
            @Path("id") int movieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @FormUrlEncoded
    @POST("http://54.245.220.198/chomokademo/api/movie")
    Call<String> postFavouriteMovieToApi(
            @Header("Authorization") String authKey,
            @Field("data") String data
    );

}
