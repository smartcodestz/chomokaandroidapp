package tz.co.smartcodes.bongoflix.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import tz.co.smartcodes.bongoflix.database.models.Movie;

@Database(entities = {Movie.class}, version = 1, exportSchema = false)
public abstract class BongoFlixDatabase extends RoomDatabase {
    public abstract BongoFlixDaoAccess daoAccess() ;
}
