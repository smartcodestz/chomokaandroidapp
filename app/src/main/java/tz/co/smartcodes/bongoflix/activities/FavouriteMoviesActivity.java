package tz.co.smartcodes.bongoflix.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import tz.co.smartcodes.bongoflix.R;

public class FavouriteMoviesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_movies);
    }
}
