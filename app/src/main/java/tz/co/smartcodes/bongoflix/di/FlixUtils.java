package tz.co.smartcodes.bongoflix.di;

import retrofit2.Retrofit;

public interface FlixUtils {

    Retrofit getRetrofit();
}
