package tz.co.smartcodes.bongoflix.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import tz.co.smartcodes.bongoflix.database.models.Movie;

@Dao
public interface BongoFlixDaoAccess {

    @Insert
    void insertOnlySingleMovie(Movie movie);

    @Insert
    void insertMultipleMovies(List<Movie> movies);

    @Query("SELECT * FROM Movie WHERE id = :movieId")
    Movie fetchOneMoviesbyMovieId(int movieId);

    @Update
    void updateMovie(Movie movie);

    @Delete
    void deleteMovie(Movie movie);
}