package tz.co.smartcodes.bongoflix;

import android.app.Application;

import com.facebook.stetho.Stetho;

import tz.co.smartcodes.bongoflix.di.BongoFlixComponent;
import tz.co.smartcodes.bongoflix.di.DaggerBongoFlixComponent;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;

public class BongoFlix extends Application {

    private BongoFlixComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        component = createComponent();
    }

    public BongoFlixComponent getComponent() {
        return component;
    }

    private BongoFlixComponent createComponent() {
        return DaggerBongoFlixComponent
                .builder()
                .bongoFlixUtils(new BongoFlixUtils())
                .build();
    }
}
