package tz.co.smartcodes.bongoflix.interfaces;

import java.util.List;

import tz.co.smartcodes.bongoflix.models.Movie;

public interface OnGetMoviesCallback {

    void onSuccess(List<Movie> movies);

    void onError();
}
