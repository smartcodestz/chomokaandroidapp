package tz.co.smartcodes.bongoflix.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.activities.MainActivity;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;

public class TourScreenTwo extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tour_screen_two, container, false);
        ImageView getStartedButton = view.findViewById(R.id.get_started);

        getStartedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = BongoFlixUtils.getSharedPreferences(getContext());
                prefs.edit().putBoolean("not_first_run", true).apply();
                getActivity().startActivity(
                        new Intent(getContext(), MainActivity.class)
                );
                getActivity().finish();
            }
        });

        return view;
    }
}
