package tz.co.smartcodes.bongoflix.utils;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import tz.co.smartcodes.bongoflix.di.FlixUtilsImplementation;
import tz.co.smartcodes.bongoflix.FlixUtils;

@Module
public class BongoFlixUtils {

    private static final String TMDB_BASE_URL = "https://api.themoviedb.org/3/";

    public static final String TMDB_API_KEY = "d25694d8332943d8f0d773642f7648ad";

    @Provides
    @Singleton
    public static FlixUtils provideFlixUtils() {
        return new FlixUtilsImplementation();
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences("tz.co.smartcodes.bongoflix.prefs", Context.MODE_PRIVATE);
    }

}
