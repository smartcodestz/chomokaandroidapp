package tz.co.smartcodes.bongoflix.di;

import javax.inject.Singleton;
import dagger.Component;
import tz.co.smartcodes.bongoflix.activities.MainActivity;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;

@Singleton
@Component(modules = BongoFlixUtils.class)
public interface BongoFlixComponent {

    void inject(MainActivity mainActivity);

}