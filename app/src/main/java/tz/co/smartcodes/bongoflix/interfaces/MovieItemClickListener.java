package tz.co.smartcodes.bongoflix.interfaces;

import android.view.View;

public interface MovieItemClickListener {
    void onItemClick(View view, int position);
}