package tz.co.smartcodes.bongoflix.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.parceler.Parcels;
import org.reactivestreams.Publisher;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.processors.PublishProcessor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tz.co.smartcodes.bongoflix.BongoFlix;
import tz.co.smartcodes.bongoflix.FlixUtils;
import tz.co.smartcodes.bongoflix.listeners.EndlessScrollViewListener;
import tz.co.smartcodes.bongoflix.R;
import tz.co.smartcodes.bongoflix.adapters.PopularMoviesAdapter;
import tz.co.smartcodes.bongoflix.interfaces.MovieItemClickListener;
import tz.co.smartcodes.bongoflix.interfaces.OnGetMoviesCallback;
import tz.co.smartcodes.bongoflix.models.Movie;
import tz.co.smartcodes.bongoflix.models.MoviesResponse;
import tz.co.smartcodes.bongoflix.api.TmdbNetworkInterface;
import tz.co.smartcodes.bongoflix.utils.BongoFlixUtils;


public class MainActivity extends AppCompatActivity {

    TmdbNetworkInterface api;

    PopularMoviesAdapter adapter;
    GridLayoutManager layoutManager;

    private boolean loading = false;
    private int pageNumber = 1;
    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishProcessor<Integer> paginator = PublishProcessor.create();

    RecyclerView movieListRecyclerView;
    ProgressBar progressBar, progressBarPaginate;

    @Inject
    FlixUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movieListRecyclerView = findViewById(R.id.movie_list);

        ((BongoFlix) getApplication()).getComponent().inject(this);

        progressBar = findViewById(R.id.progress_bar);
        progressBarPaginate = findViewById(R.id.progress_paginate);

        layoutManager = new GridLayoutManager(this, 3);
        movieListRecyclerView.setLayoutManager(layoutManager);

        EndlessScrollViewListener endlessScrollViewListener = new EndlessScrollViewListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                getMovies(new OnGetMoviesCallback() {
                    @Override
                    public void onSuccess(final List<Movie> movies) {
                        adapter.addMovies(movies);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError() {
                        Toast.makeText(MainActivity.this, "Check your Internet connection.", Toast.LENGTH_SHORT).show();
                    }
                }, page);
            }
        };

        // Retrofit retrofit = TmdbNetworkClient.getRetrofit();
        api = utils.getRetrofit().create(TmdbNetworkInterface.class);

        getMovies(new OnGetMoviesCallback() {
            @Override
            public void onSuccess(final List<Movie> movies) {
                adapter = new PopularMoviesAdapter(getApplicationContext(), movies);

                adapter.setItemClickListener(new MovieItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getApplicationContext(), MovieDetailsActivity.class);
                        intent.putExtra("movie", Parcels.wrap(movies.get(position)));
                        startActivity(intent);
                    }
                });
                movieListRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onError() {
                Toast.makeText(MainActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
            }
        }, 1);

        movieListRecyclerView.setOnScrollListener(endlessScrollViewListener);

        // setUpLoadMoreMoviesListener();
        // subscribeForMoviesData();

    }

    private void getMovies(final OnGetMoviesCallback callback, final int page) {

        progressBar.setVisibility(View.VISIBLE);

        api.getPopularMovies(BongoFlixUtils.TMDB_API_KEY, "en-US", page)
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MoviesResponse> call, @NonNull Response<MoviesResponse> response) {
                        if (response.isSuccessful()) {
                            MoviesResponse moviesResponse = response.body();
                            if (moviesResponse != null && moviesResponse.getMovies() != null) {
                                progressBar.setVisibility(View.GONE);
                                callback.onSuccess(moviesResponse.getMovies());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
                        callback.onError();
                    }
                });
    }

    private void setUpLoadMoreMoviesListener() {
        movieListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();

                // Toast.makeText(getApplicationContext(), String.valueOf(lastVisibleItem), Toast.LENGTH_LONG).show();

                if (!loading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {

                    pageNumber++;

                    paginator.onNext(pageNumber);
                    loading = true;

                }
            }
        });
    }

    private void subscribeForMoviesData() {
        // progressBarPaginate.setVisibility(View.VISIBLE);

        Disposable disposable = paginator
                .onBackpressureDrop()
                .concatMap(new Function<Integer, Publisher<MoviesResponse>>() {
                    @Override
                    public Publisher<MoviesResponse> apply(@NonNull Integer page) throws Exception {
                        loading = true;
                        progressBar.setVisibility(View.VISIBLE);
                        return fetchMoviesFromNetwork(page);

                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<MoviesResponse>() {
                    @Override
                    public void accept(@NonNull MoviesResponse moviesResponse) throws Exception {
                        adapter.addMovies(moviesResponse.getMovies());
                        adapter.notifyDataSetChanged();
                        loading = false;
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

        compositeDisposable.add(disposable);
        paginator.onNext(pageNumber);

    }

    private Flowable<MoviesResponse> fetchMoviesFromNetwork(final int pageNumber) {
        return Flowable.just(true)
                .map(new Function<Boolean, MoviesResponse>() {
                    @Override
                    public MoviesResponse apply(@NonNull Boolean value) {
                        final MoviesResponse[] moviesResponse = {null};
                        api.getPopularMovies(BongoFlixUtils.TMDB_API_KEY, "en-US", pageNumber)
                                .enqueue(new Callback<MoviesResponse>() {
                                    @Override
                                    public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                                        moviesResponse[0] = response.body();
                                    }

                                    @Override
                                    public void onFailure(Call<MoviesResponse> call, Throwable t) {

                                    }
                                });
                        return moviesResponse[0];
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }

}
